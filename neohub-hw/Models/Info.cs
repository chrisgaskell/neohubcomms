﻿using System;
using System.Collections.Generic;
using System.Text;

namespace neohub_hw.Models
{

    public class Info
    {
        public DeviceInfo[] devices { get; set; }
    }

    public class DeviceInfo
    {
        public bool AWAY { get; set; }
        public bool COOLING { get; set; }
        public bool COOLING_ENABLED { get; set; }
        public int COOLING_TEMPERATURE_IN_WHOLE_DEGREES { get; set; }
        public bool COOL_INP { get; set; }
        public string COUNT_DOWN_TIME { get; set; }
        public bool CRADLE_PAIRED_TO_REMOTE_SENSOR { get; set; }
        public bool CRADLE_PAIRED_TO_STAT { get; set; }
        public int CURRENT_FLOOR_TEMPERATURE { get; set; }
        public string CURRENT_SET_TEMPERATURE { get; set; }
        public string CURRENT_TEMPERATURE { get; set; }
        public bool DEMAND { get; set; }
        public int DEVICE_TYPE { get; set; }
        public bool ENABLE_BOILER { get; set; }
        public bool ENABLE_COOLING { get; set; }
        public bool ENABLE_PUMP { get; set; }
        public bool ENABLE_VALVE { get; set; }
        public bool ENABLE_ZONE { get; set; }
        public bool FAILSAFE_STATE { get; set; }
        public bool FAIL_SAFE_ENABLED { get; set; }
        public bool FLOOR_LIMIT { get; set; }
        public bool FULLPARTIAL_LOCK_AVAILABLE { get; set; }
        public bool HEATCOOL_MODE { get; set; }
        public bool HEATING { get; set; }
        public int HOLD_TEMPERATURE { get; set; }
        public string HOLD_TIME { get; set; }
        public bool HOLIDAY { get; set; }
        public int HOLIDAY_DAYS { get; set; }
        public int HUMIDITY { get; set; }
        public bool LOCK { get; set; }
        public string LOCK_PIN_NUMBER { get; set; }
        public bool LOW_BATTERY { get; set; }
        public string MAX_TEMPERATURE { get; set; }
        public string MIN_TEMPERATURE { get; set; }
        public int MODULATION_LEVEL { get; set; }
        public string NEXT_ON_TIME { get; set; }
        public bool OFFLINE { get; set; }
        public bool OUPUT_DELAY { get; set; }
        public int OUTPUT_DELAY { get; set; }
        public bool PREHEAT { get; set; }
        public string PREHEAT_TIME { get; set; }
        public string PROGRAM_MODE { get; set; }
        public bool PUMP_DELAY { get; set; }
        public bool RADIATORS_OR_UNDERFLOOR { get; set; }
        public string SENSOR_SELECTION { get; set; }
        public int SET_COUNTDOWN_TIME { get; set; }
        public bool STANDBY { get; set; }
        public STAT_MODE STAT_MODE { get; set; }
        public bool TEMPERATURE_FORMAT { get; set; }
        public bool TEMP_HOLD { get; set; }
        public bool TIMECLOCK_MODE { get; set; }
        public bool TIMER { get; set; }
        public bool TIME_CLOCK_OVERIDE_BIT { get; set; }
        public int ULTRA_VERSION { get; set; }
        public int VERSION_NUMBER { get; set; }
        public int WRITE_COUNT { get; set; }
        public bool ZONE_1PAIRED_TO_MULTILINK { get; set; }
        public bool ZONE_1_OR_2 { get; set; }
        public bool ZONE_2_PAIRED_TO_MULTILINK { get; set; }
        public string device { get; set; }
    }

    public class STAT_MODE
    {
        public bool _4_HEAT_LEVELS { get; set; }
        public bool MANUAL_OFF { get; set; }
        public bool THERMOSTAT { get; set; }
    }

}
