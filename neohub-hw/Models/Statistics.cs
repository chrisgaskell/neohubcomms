﻿using System;
using System.Collections.Generic;
using System.Text;

namespace neohub_hw.Models
{
    public class Statistics
    {
        public Records houserecords { get; set; }
        public Preheatrecords preheatrecords { get; set; }
        public Preheatstarts preheatstarts { get; set; }
        public Roomrecords roomrecords { get; set; }
        public Runrecords runrecords { get; set; }
    }

    public class Records
    {
        public string highestmonthtemp { get; set; }
        public string highestweektemp { get; set; }
        public string highestyeartemp { get; set; }
        public string lowestmonthtemp { get; set; }
        public string lowestweektemp { get; set; }
        public string lowestyeartemp { get; set; }
    }

    public class Preheatrecords
    {
        public string monthrun { get; set; }
        public string weekrun { get; set; }
        public string yearrun { get; set; }
    }

    public class Preheatstarts
    {
    }

    public class Roomrecords
    {
        public Records Bathroom { get; set; }
        public Records Bedroom2 { get; set; }
        public Records Bedroom3 { get; set; }
        public Records Bedroom4 { get; set; }
        public Records Hallway { get; set; }
        public Records Kitchen { get; set; }
        public Records Lounge { get; set; }
        public Records MasterBedroom { get; set; }
        public Records Office { get; set; }
        public Records Utility { get; set; }
    }

    public class Runrecords
    {
        public string monthrun { get; set; }
        public string weekrun { get; set; }
        public string yearrun { get; set; }
    }

}
